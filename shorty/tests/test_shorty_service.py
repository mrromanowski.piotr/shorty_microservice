from shorty.short_links import short_urls

shorty = short_urls()

def test_status_code_retuned():
    """
    Given the provider name is not specified should retun a http status code 200 of first available
    """
    response = shorty.call_provider(provider="",url="http://dummy_url.com")
    assert response['status_code'] == (200 or 201 or 202)

def test_fallback():
    """
    Given the provider name is incorrect service should retun a valid short_link using available provider
    """
    response = shorty.call_provider(provider="gibbrish",url="http://dummy_url.com")
    assert "http" in response['short_link'].decode()

def test_specified_provider():
    """
    if provider specified and available use it
    """
    response = shorty.call_provider(provider="bitly",url="http://dummy_url.com")
    assert "bit.ly" in response['short_link'].decode()

def test_provider_gives_error_status():
    """
    breaking the call by not providing correct params to receive a non 200 code (should get 400) 
    """
    provider = {"call_auth" :{"Authorization": "Bearer a3b654a90df76600025f212462846c0639ea121a", "Content-Type": "application/json"}, "req_params":{"long_url": "", "domain": "bit.ly"},"submit_url":"https://api-ssl.bitly.com/v4/shorten"}
    response = shorty.call_bitly(auth=provider['call_auth'],params=provider['req_params'],long_url="",submit_url=provider['submit_url'])
    status_code = response['status_code']
    assert status_code != 200

def test_provider_gives_fqdn_hint():
    """
    breaking the call by not providing correct params to receive a FQDN prompt
    """
    provider = {"call_auth" :{"Authorization": "Bearer a3b654a90df76600025f212462846c0639ea121a", "Content-Type": "application/json"}, "req_params":{"long_url": "", "domain": "bit.ly"},"submit_url":"https://api-ssl.bitly.com/v4/shorten"}
    response = shorty.call_bitly(auth=provider['call_auth'],params=provider['req_params'],long_url="http://not_fqdn_link",submit_url=provider['submit_url'])
    assert "have u used FQDN?" in response['link'].decode()
