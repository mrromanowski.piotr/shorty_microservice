import requests

class short_urls:

	switcher = {
	"bitly": {"call_auth" :{"Authorization": "Bearer a3b654a90df76600025f212462846c0639ea121a", "Content-Type": "application/json"}, "req_params":{"long_url": "", "domain": "bit.ly"}, "is_live_url":"https://api-ssl.bitly.com/v4/user","submit_url":"https://api-ssl.bitly.com/v4/shorten","provider_name":"bitly"},
	"tinyurl": {"call_auth" :{"Authorization": "Bearer ujWoHx2FQZUVIVOWr2XJAsa3lrHbLlR3PpZUSo1nxfkLXAVz2xuLVrdABMf2", "Content-Type": "application/json"}, "req_params":{"url":"","domain": "tiny.one"},"is_live_url":"https://tinyurl.com/openapi/v2.json","submit_url":"https://api.tinyurl.com/create","provider_name":"tinyurl"}
	}

	def call_bitly(self, auth, params, long_url, submit_url): #vendor specific call
		header = auth
		params['long_url']=long_url
		response = requests.post(submit_url,json=params, headers=header)
		data = response.json()
		if 'link' in data.keys(): short_link = data['link']
		else: short_link = "have u used FQDN?"
		call_response = {"link":short_link.encode("utf-8"), "status_code":response.status_code}
		return call_response #short_link.encode("utf-8")


	def call_tinyurl(self, auth, params, long_url, submit_url): #vendor specific call
		header = auth
		params['url']=long_url
		response = requests.post(submit_url,json=params, headers=header)
		data = response.json()
		short_link = data['data']['tiny_url']
		call_response = {"link":short_link.encode("utf-8"), "status_code":response.status_code}
		return call_response #short_link.encode("utf-8")


	def test_provider_available(self,provider):					
		r = requests.get(provider['is_live_url'],headers=provider['call_auth'])
		return r.status_code


	def fallback_provider(self):
		find_fallback_provider = iter(self.switcher)									#iter to find fallback
		for provider in find_fallback_provider:
			if self.test_provider_available(self.switcher.get(provider)) == 200:
				provider_auth = self.switcher.get(provider)
				return provider_auth			
			else:
				continue

	
	def choose_provider(func_passed_in):
		def wrapper(self,**kwargs):
			if self.switcher.get(kwargs['provider']) is not None: 							#test selection
				if self.test_provider_available(self.switcher.get(kwargs['provider'])) == 200:	#if live 
					provider_details = self.switcher.get(kwargs['provider'])
				else:
					provider_details = self.fallback_provider()									#if not find fallback
			else:
				provider_details = self.fallback_provider()										#if no or bad selection find fallback

			response = func_passed_in(self,**dict(kwargs,provider_auth=provider_details['call_auth'], provider_name=provider_details['provider_name'], params=provider_details['req_params'], submit_url=provider_details['submit_url'])) #lets craft a call 
			return response
			print("Ended")

		return wrapper


	@choose_provider #pre checks
	def call_provider(self,provider, url, provider_auth="", provider_name="", params="", submit_url=""): #main call interface
		provider_calls = {
			"bitly":self.call_bitly,
			"tinyurl":self.call_tinyurl
		}
		call = provider_calls.get(provider_name)
		short_link = call(auth=provider_auth, params=params, long_url=url,  submit_url=submit_url)
		#response = {"requested":provider, "url":long_url, "provider_auth":provider_auth, "delivery_provider":provider_name, "params":params, "submit_url":submit_url, "short_link":short_link}
		response = {"url":url, "short_link":short_link['link'],"status_code":short_link['status_code']}
		return response






