from flask import Blueprint, jsonify, request
from .short_links import short_urls
import requests


api = Blueprint('api', __name__)

@api.route('/ver', methods=['GET'])
def version():
    return jsonify({"version": "1.0", "author":"Piotr Romanowski","compiled":"22-02-2022 17:53"})

@api.route('/shortlinks', methods=['POST'])
def create_shortlink():
    j_input = request.get_json()
    shorty = short_urls()
    short_link = shorty.call_provider(provider=j_input['provider'],url=j_input['url'])

    return jsonify({
        "url":str(short_link['url']),
        "link":str(short_link['short_link'].decode()),
        "status_code":str(short_link['status_code'])})
